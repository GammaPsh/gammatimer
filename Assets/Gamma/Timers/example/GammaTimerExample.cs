﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using Gamma.Timers;

public class GammaTimerExample : MonoBehaviour {

	public void GammaTimerExampleClicked(InputHelper inputHelper)
	{
		float seconds;
		if(float.TryParse(inputHelper.inputField.text, out seconds)){

			Timer timer = new Timer(TimeSpan.FromSeconds(seconds), ()=>{
				inputHelper.timeLabel.text = "Timer is Up";
			});
			timer.Start();

			inputHelper.timeLabel.text = string.Format("Timer Started: pening {0} sek", seconds);

		}else{
			inputHelper.timeLabel.text = "Input no a valid Float";
		}
	}

	Intervall intervall;
	public void GammaIntervallExampleClicked(InputHelper inputHelper)
	{
		if(intervall == null){
			float seconds;
			if(float.TryParse(inputHelper.inputField.text, out seconds)){

				intervall = new Intervall(TimeSpan.FromSeconds(seconds), 0, (uint intervallTime)=>{
					inputHelper.timeLabel.text = string.Format("{0} sek Intervall Elapsed  {1} times", seconds, intervallTime);
				},true);
				intervall.Start();

				inputHelper.timeLabel.text = string.Format("Timer Started: pening {0} sek", seconds);
			}else{
				inputHelper.timeLabel.text = "Input no a valid Float";
			}
		}else{
			inputHelper.timeLabel.text = "Intervall Stopped";
			intervall.Dispose();
			intervall = null;
		}
	}

	public void GammaProgressTimerClicked(InputHelper inputHelper)
	{
		float seconds;
		if(float.TryParse(inputHelper.inputField.text, out seconds)){

			Progress progressTimer = new Progress(TimeSpan.FromSeconds(seconds), (progress)=>{
				inputHelper.timeLabel.text = string.Format("Timer of {0} sek, progress: {1}", seconds, progress);
			});
			progressTimer.Start();

			inputHelper.timeLabel.text = string.Format("Started Timer of {0} sek", seconds);

		}else{
			inputHelper.timeLabel.text = "Input no a valid Float";
		}
	}
}
